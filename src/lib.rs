extern crate wasm_bindgen;
// extern crate rusqlite;

use wasm_bindgen::prelude::*;
use config::Config;

pub mod config;
// pub mod db;

#[wasm_bindgen]
extern {

}

#[wasm_bindgen(start)]
pub fn main() {
    let config: Config = Config::default();
    // db::init(&config).expect("db init failed");
}
