use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub struct Config {
    images_folder: String,
    db_name: String,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            images_folder: "/images".to_string(),
            db_name: "images.db".to_string(),
        }
    }
}

#[wasm_bindgen]
impl Config {
    pub fn new() -> Config {
        Config::default()
    }

    pub fn get(&self) -> String {
        self.images_folder.clone()
    }

    pub fn set(&mut self, val: &str) {
        self.images_folder = val.to_string();
    }

    pub fn get_db_name(&self) -> String {
        self.db_name.clone()
    }
}
